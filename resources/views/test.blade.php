@extends('layouts')
    <!DOCTYPE html>

<html>

<head>

    <title>Laravel 7 - Populate select box from database example</title>

</head>

<body>
@section('content')

    {{ Form::open(array('url' => '#', 'id' => 'wilayah')) }}

    <table width="500px" align="center">
        <tr>
            <td>{{ Form::label('propinsi', 'Propinsi') }}</td>
            <td>
                : {{ Form::select('propinsi', $propinsi, null, array('id' => 'sPropinsi', 'style'=>'width: 200px'))  }}</td>
        </tr>
        <tr>
            <td>{{ Form::label('kota', 'Kabupaten/Kota') }}</td>
            <td>: {{ Form::select('kota', array(), null, array('id' => 'sKota', 'style'=>'width: 200px'))  }}</td>
        </tr>
        <tr>
            <td>{{ Form::label('kecamatan', 'Kecamatan') }}</td>
            <td>
                : {{ Form::select('kecamatan', array(), null, array('id' => 'sKecamatan', 'style'=>'width: 200px'))  }}</td>
        </tr>
        <tr>
            <td>{{ Form::label('desa', 'Desa / Kelurahan') }}</td>
            <td>: {{ Form::select('desa', array(), null, array('id' => 'sDesa', 'style'=>'width: 200px'))  }}</td>
        </tr>
    </table>

    {{ Form::close() }}

</body>

@stop
</html>
