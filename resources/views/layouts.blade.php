<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dropdown Dependency - Studi kasus Pemilihan Propinsi Indonesia</title>
    <script src="https://code.jquery.com/jquery-3.5.1.js"
            integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
</head>
<body>
@yield('content')
<script type="text/javascript">
    $(document).ready(function () {
        @yield('script')
    });
</script>
</body>
</html>
