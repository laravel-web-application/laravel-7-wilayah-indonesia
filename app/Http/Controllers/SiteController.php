<?php

namespace App\Http\Controllers;

use App\Models\District;
use App\Models\Province;
use App\Models\Regency;
use App\Models\Village;
use Illuminate\Support\Facades\View;

class SiteController extends Controller
{
    public function index()
    {
        $propinsi = array('' => '');
        foreach (Province::all() as $row)
            $propinsi[$row->id] = $row->name;

        return View::make('index', array(
            'propinsi' => $propinsi
        ));
    }

    public function postData()
    {
        switch (request()->get('type')):
            case 'kota':
                $return = '<option value=""></option>';
                foreach (Regency::where('province_id', request()->get('type'))->get() as $row)
                    $return .= "<option value='$row->province_id'>$row->name</option>";
                return $return;
                break;
            case 'kecamatan':
                $return = '<option value=""></option>';
                foreach (District::where('kota_id', request()->get('type'))->get() as $row)
                    $return .= "<option value='$row->id'>$row->kecamatan</option>";
                return $return;
                break;
            case 'desa':
                $return = '<option value=""></option>';
                foreach (Village::where('kecamatan_id', request()->get('type'))->get() as $row)
                    $return .= "<option value='$row->id'>$row->desa</option>";
                return $return;
                break;
        endswitch;
    }

    public function provinces()
    {
        $provinces = Province::pluck('id', 'name');
        return view('test', compact('provinces'));
    }

    public function getDistricts()
    {
        // Get Kabupaten/Kota dari sebuah Provinsi
        $province = Province::all();
        if ($province->hasDistrictId([32], true)) {
//        $regencies = Regency::all();
            $regencies = $province->regencies;
        }
        return response()->json($regencies);
    }
}
