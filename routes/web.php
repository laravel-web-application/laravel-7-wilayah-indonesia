<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'SiteController@index');
//Route::resource('site', 'SiteController');
Route::post('/site/data', 'SiteController@postData');

Route::get('/provinces', 'SiteController@provinces');

Route::get('/indonesia', 'CountryController@provinces');

Route::get('/regencies', 'CountryController@regencies');

Route::get('/districts', 'CountryController@districts');

Route::get('/village', 'CountryController@villages');
