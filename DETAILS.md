# Laravel 7 Wilayah Indonesia
### Membuat dependant dropdown/chain dropdown Wilayah Indonesia dengan Laravel 7

Based on https://github.com/azishapidin/indoregion


#### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-7-wilayah-indonesia.git`
2. Go inside the folder: `cd laravel-7-wilayah-indonesia`
3. Run `composer install`
4. Run `cp .env.example .env` then set your DB credential
5. Run `php artisan migrate`
6. Run `php artisan serve`
7. Open your favorite browser: http://localhost:8000/indonesia

### Screen shot

Wilayah Indonesia

![Wilayah Indonesia](img/indonesia.png "Wilayah Indonesia")
